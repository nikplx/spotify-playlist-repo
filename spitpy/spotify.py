from time import gmtime
import uuid

class User:
    """ Spotify User entity """
    def __init__(self, username, playlists = []):
        self.username = username
        self.playlists = playlists
    
    def getCurrentPlaylists(self, session):
        if not any(self.playlists):
            self.playlists = SpotifyAPI.currenUserPlaylists(session)
        else:
            for playlist in self.playlists:
               playlist.getCurrentTracks(session)

class Playlist:
    """ Spotify Playlist Queried from Web API """
    def __init__(self, spotify_id, name, tracks = [], initialTracks = [], transactions = []):
        self.spotify_id = spotify_id
        self.name = name
        self.tracks = tracks
        self.initialTracks = initialTracks
        self.transactions = sorted(transactions, key=lambda t : t.time, reverse=True)

    def getCurrentTracks(self, session):
        self.tracks = SpotifyAPI.currentUserPlaylistTracks(session, self.spotify_id)

    def snapshot(self):
        if not any(self.tracks):
            raise Exception('Snapshot error: Can not take snapshot without any tracks loaded.')
        elif not any(self.transactions):
            self.transactions.append(
                Transaction(
                    remove=set(self.initialTracks).difference(set(self.tracks)),
                    add=set(self.tracks).difference(set(self.initialTracks))
                )
            )
        else:
            lastKnownTracks = set(self.getTracksAtTransaction(self.transactions[0]))
            delta_initial_current = set(self.initialTracks).symmetric_difference(set(self.tracks))
            delta_last_current = lastKnownTracks.symmetric_difference(set(self.tracks))

            if not any(delta_initial_current):
                print("Snapshot info: Playlist already up to date.")
            elif any(delta_last_current):
                try:
                    self.transactions.append(
                        Transaction(
                            remove=lastKnownTracks.difference(set(self.tracks)),
                            add=set(self.tracks).difference(lastKnownTracks)
                        )
                    )
                    print("Snapshot: Created new Snapshot" + self.name)
                except Exception as error:
                    print(error)
            else:
                pass

    def restore(self, transaction):
        return

    def getTracksAtTransaction(self, transaction):
        transaction_index = self.transactions.index(transaction) + 1
        transactions_apply = self.transactions[0:transaction_index]
        return self.applyTransactions(self.initialTracks.copy(), transactions_apply)
    
    def applyTransactions(self, playlist, transactions):
        for transaction in transactions:
            playlist.extend(transaction.add)

            for track in transaction.remove:
                playlist.remove(track)
        return playlist
    
    def cleanTransactions(self):
        for transaction in self.transactions:
            if not any(transaction.add) and not any(transaction.remove):
                self.transactions.remove(transaction)

class Track:
    """ Spotify Track """
    def __init__ (self, spotify_id, name, artist):
        self.spotify_id = spotify_id
        self.name = name
        self.artist = artist

    def __hash__(self):
        return hash((self.spotify_id, self.name, self.artist))
    
    def __eq__(self, other):
        if isinstance(other, Track):
            return (self.spotify_id, self.name, self.artist) == (other.spotify_id, other.name, other.artist)
        else:
            return False

class Transaction:
    """ Playlist Transaction """
    def __init__ (self, remove = [], add = [], time = gmtime(), transaction_id = uuid.uuid4().hex):
        self.transaction_id = transaction_id
        self.remove = remove
        self.add = add
        self.time = time
    
class SpotifyAPI:
    """ Wrapper for the Spotipy API Methods """
    @staticmethod
    def currentUserPlaylistTracks(session, playlist_id):
        tracks_from_spotify = session.spotify.user_playlist_tracks(
            session.current_user.username,
            playlist_id
        )
        
        tracks = []
        while(tracks_from_spotify['next']):
            results = session.spotify.next(tracks_from_spotify)
            tracks_from_spotify['items'].extend(results['items'])
            if len(results) < 100:
                break
                

        for item in tracks_from_spotify['items']:
            tracks.append(
                Track(
                    item['track']['id'],
                    item['track']['name'],
                    item['track']['artists'][0]['name']
                )
            )
        return tracks

    @staticmethod
    def currenUserPlaylists(session):
        #TODO Playlist haben ein Limit von 50, next Konstrukt benutzen
        playlists_from_spotify = session.spotify.current_user_playlists()
        playlists = []
        for item in playlists_from_spotify['items']:
            playlists.append(
                Playlist(
                    item['id'],
                    item['name'],
                    SpotifyAPI.currentUserPlaylistTracks(session, item['id'])
                )
            )
        return playlists

    @staticmethod
    def currentUserPlaylistReplaceTracks(session):
        return #Spotify.user_playlist_replace_tracks()