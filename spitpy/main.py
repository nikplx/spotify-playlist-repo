#!/usr/bin/python3
import configparser
import json
import argparse
import spotipy
import spotipy.util as util
import os
import click

from utils import DataHandler, SerializationHandler, Session
from spotify import User, SpotifyAPI
from time import strftime, strptime

confparser = configparser.ConfigParser()
confparser.read('spotify.ini')
config = confparser['Spotify']

output_format_string = '|{:<35}|{:<35}|{:<35}|'
output_separator = '-' * (3 * 35 + 4)

session = None

@click.group()
def spit():
    global session
    session  = Session(
        config['username'],
        config['scope'],
        config['client_id'],
        config['client_secret'],
        config['redirect_uri'],
        config['data_directory']
    )
    session.current_user.getCurrentPlaylists(session)

@spit.command()
@click.option('--all-playlists', '-A', is_flag=True)
def list(all_playlists):
    click.echo(
        click.style(
            output_format_string.format('Playlist Name', 'Playlist ID', 'Number of Transactions'),
            bg='blue',
            fg='black'
        ))    
    click.echo(output_separator)

    if all_playlists:
        for playlist in session.current_user.playlists:
            click.echo(output_format_string.format(playlist.name, playlist.spotify_id, len(playlist.transactions)))
    click.echo(output_separator)

@spit.command()
@click.option('--playlist-name', '-n', prompt='Name or Spotify ID of the Playlist', required=True)
@click.option('--show-id', '-i', is_flag=True)
def show(playlist_name, show_id):
    for playlist in session.current_user.playlists:
        if(playlist_name in playlist.name or playlist_name in playlist.spotify_id):
            if (len(playlist.transactions) > 0):
                click.echo(click.style(playlist.name + ':', underline=True, bold=True))
                click.echo(
                    click.style(
                        output_format_string.format('Transaction Date', 'Artist', 'Song'),
                        fg='black',
                        bg='blue'
                    ))
                click.echo(output_separator)

                for transaction in playlist.transactions:
                    if show_id:
                        click.echo(output_format_string.format(transaction.transaction_id, '', ''))
                    click.echo(output_format_string.format(strftime("%d.%m.%Y - %H:%M", transaction.time), '', ''))
                    for track in transaction.add:
                        click.echo(
                            output_format_string.format(
                                '+',
                                track.artist,
                                track.name
                            )
                        )
                    for track in transaction.remove:
                        click.echo(
                            output_format_string.format(
                                '-',
                                track.artist,
                                track.name
                            )
                        )
                click.echo(output_separator)
            else:
                click.echo(click.style('No Transactions for Playlist with name ' + playlist.name, bold=True))

@spit.command()
@click.argument('playlists', nargs=-1, required=False)
@click.option('--all-playlists', '-A', is_flag=True)
@click.option('--save', '-s', is_flag=True)
@click.option('--path', '-p')
def snapshot(playlists, all_playlists, save, path):
    if all_playlists:
        click.echo('Taking snapshots of all Playlists from User ' + session.current_user.username)
        for playlist in session.current_user.playlists:
            click.echo('Snapshoting Playlist ' + playlist.name)
            playlist.snapshot()
    else:
        click.echo('Taking Snapshots of Playlist: ' + playlists)
        for playlist in session.current_user.playlists:
            if playlist.name in playlists:
                click.echo('Snapshoting Playlist: ' + playlist.name)
                playlist.snapshot()
    
    if save:
        file_name = os.path.join(path if path else session.data_directory, session.current_user.username + ".json")
        click.echo('Writing User Data for User ' + session.current_user.username + ' to File ' + file_name)
        DataHandler.writeToFile(
            file_name,
            session.current_user
        )

@spit.command()
@click.option('--all-playlists', '-A', is_flag=True)
@click.option('--ask-for-permission', '-a', is_flag=True)
def clean(all_playlists, ask_for_permission):
    if all_playlists:
        click.echo("Cleaning up all Playlists now:")
        for playlist in session.current_user.playlists:
            if not any(playlist.transactions):
                #TODO implement the ask for permission
                click.echo("No Transactions to clean up in" + playlist.name)
            else:
                playlist.cleanTransactions()

if __name__ == '__main__':
    try:
        spit()
    except SystemExit:
        pass