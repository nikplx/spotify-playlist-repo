from spotify import Playlist, User, Track, Transaction
from time import gmtime, strftime, strptime
from os import listdir, path, mkdir
from spotipy import util, Spotify
import json
import click
import sys

class SerializationHandler:
    """ Serializer / Deserializer for all Spotify Entities """
    @staticmethod
    def deserialize(item):
        if '__playlist__' in item:
            return Playlist(
                item['id'],
                item['name'],
                [SerializationHandler.deserialize(track) for track in item['tracks']],
                [SerializationHandler.deserialize(track) for track in item['initial_tracks']],
                [SerializationHandler.deserialize(transaction) for transaction in item['transactions']]
            )
        elif '__track__' in item:
            return Track(
                item['id'],
                item['name'],
                item['artist']
            )
        elif '__user__' in item:
            return User(
                item['id'],
                [SerializationHandler.deserialize(playlist) for playlist in item['playlists']]
            )
        elif '__transaction__' in item:
            return Transaction(
                [SerializationHandler.deserialize(track) for track in item['remove']],
                [SerializationHandler.deserialize(track) for track in item['add']],
                strptime(item['time'], "%d.%m.%Y-%H:%M:%S"),
                item['id']
            )
        else:
            raise Exception('Invalid object error: no object fits deserialize method.')
    
    @staticmethod
    def serialize(item):
        if isinstance(item, Playlist):
            return {
                '__playlist__': True,
                'id': item.spotify_id,
                'name': item.name,
                'tracks': [SerializationHandler.serialize(track) for track in item.tracks],
                'initial_tracks': [SerializationHandler.serialize(track) for track in item.initialTracks],
                #TODO Track wird irgendwie schon vorher serialisiert und liegt so in dem objekt
                'transactions': [SerializationHandler.serialize(transaction) for transaction in item.transactions]
            }
        #TODO Throws strange errors while decoding transaction -> reproduce -> add track -> snapshot -> read
        elif isinstance(item, Track):
            return {
                '__track__': True,
                'id': item.spotify_id,
                'name': item.name,
                'artist': item.artist
            }
        elif isinstance(item, User):
            return {
                '__user__': True,
                'id': item.username,
                'playlists': [SerializationHandler.serialize(playlist) for playlist in item.playlists]
            }
        elif isinstance(item, Transaction):
            return {
                '__transaction__': True,
                'remove': [SerializationHandler.serialize(track) for track in item.remove],
                'add': [SerializationHandler.serialize(track) for track in item.add],
                'time': strftime("%d.%m.%Y-%H:%M:%S", item.time),
                'id': item.transaction_id
            }
        else:
            raise Exception('Invalid item error: provided item not serializable.')
    


class DataHandler:
    @staticmethod
    def writeToFile(file, data):
        with open(file, "w") as  file:
            file_before = file
            try:   
                serialized_data = SerializationHandler.serialize(data)
                json.dump(
                    serialized_data,
                    file,
                    indent=4,
                    separators=(',', ':')
                )
            except Exception as error:
                Writer.error(error)
                file = file_before

    @staticmethod
    def readFromFile(file):
        with open(file, "r") as file:
            try:
                return SerializationHandler.deserialize(
                    json.load(file)
                )
            except:
                Writer.error('Unexpected Error loading User from file')
                pass
                

    @staticmethod
    def getUserFromData(username, data_directory):
        if not path.isdir(data_directory):
            mkdir(data_directory)
        for file in listdir(data_directory):
            if username in file:
                return DataHandler.readFromFile(path.join(data_directory, file))
        return False

class Session:
    """ Current Spotify Session """
    def __init__ (self, username, scope, client_id, client_secret, redirect_uri, data_directory):
        self.spotify = Spotify(
            auth = util.prompt_for_user_token(
                username,
                scope,
                client_id,
                client_secret,
                redirect_uri
            )
        )
                 
        self.data_directory = path.join(
            path.dirname(path.realpath(__file__)),
            data_directory
        )

        UserFromFile = DataHandler.getUserFromData(
            username,
            self.data_directory
        )
        if UserFromFile:
            self.current_user = UserFromFile
        else: 
            self.current_user = User(username)
            self.current_user.getCurrentPlaylists(self)

class Writer:
    @staticmethod
    def error(data):
        click.echo(
            click.style(
                data,
                fg='white',
                bg='red',
                bold=True
            )
        )

    @staticmethod
    def header(data):
        click.echo(
            click.style(
                data,
                fg='black',
                bg='blue'
            )
        )
    
#TODO Wrapper for click echo output 

    